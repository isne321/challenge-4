#include <iostream>
#include <ctime> //uses for seed of randoms
#include <cstdlib> //uses for randoms
#include <vector> //uses for vector
#include <algorithm> //uses for sort function
#include <cmath> //uses for calculate sd

using namespace std;

void printValue(const vector<int>&); //prints out -- O(N)
void sumValue(const vector<int>&, int&); //finds summary  -- O(N)
void maxValue(const vector<int>&, int&); //finds hightest value  -- O(N)
void minValue(const vector<int>&, int&); //finds lowest value -- O(N)
void meanValue(const vector<int>&, const int&, float&); //find mean value -- O(1)
void sortValue(vector<int>&); //sort value -- N*log2(N)
void medianValue(const vector<int>&); //find median value -- O(1)
void modeValue(const vector<int>&); //find mode value -- O(N)
void sdValue(const vector<int>&, const float&, float&); //find sd value -- O(N)
void odd_evenValue(const vector<int>&, int&, int&); //find odd/even value -- O(N)

/*
Note : some variables are constant to make me sure that the value of variable is not changed
*/

int main() {
	srand(time(0));

	float mean = 0, sd = 0;
	int sum = 0, min = 0, max = 0, odd = 0, even = 0;
	vector<int> numbers;
	int elements = (rand() % 100) + 50; //elements are between 50 - 150
	
	for (int i = 0; i < elements; i++) { // -- O(N)
		int genNum = rand() % 101; //numbers will be between 0 - 100 (101 elements)
		numbers.push_back(genNum);
	}

	cout << "There are " << elements << " elements generated." << endl;
	cout << "------------------------------------"; //seperate line
	printValue(numbers);
	cout << "------------------------------------" << endl; //seperate line
	sortValue(numbers);
	cout << "------------------------------------" << endl; //seperate line
	sumValue(numbers,sum);
	maxValue(numbers,max);
	minValue(numbers,min);
	meanValue(numbers, sum, mean);
	modeValue(numbers);
	medianValue(numbers);
	odd_evenValue(numbers, odd, even);
	sdValue(numbers, mean, sd);
	cout << "------------------------------------" << endl; //seperate line
	return 0;
}
void printValue(const vector<int>& numbers) {
	for (int i = 0; i < numbers.size(); i++) {
		if ((i % 10) == 0) {
			cout << endl; //10 value each line
		}
		cout << numbers[i] << " ";
	}
	cout << endl;
}
void sumValue(const vector<int>& numbers, int& sum) {

	for (int i = 0; i < numbers.size(); i++) {
		sum += numbers[i];
	}
	cout << "Sum is : " << sum << endl;
}
void maxValue(const vector<int>& numbers, int& max) {
	max = numbers[0];
	for (int i = 0; i < numbers.size(); i++) {
		if (numbers[i] > max) {
			max = numbers[i];
		}
	}
	cout << "Max is : " << max << endl;
}
void minValue(const vector<int>& numbers, int& min) {
	min = numbers[0];
	for (int i = 0; i < numbers.size(); i++) {
		if (numbers[i] < min) {
			min = numbers[i];
		}
	}
	cout << "Min is : " << min << endl;
}
void meanValue(const vector<int>& numbers, const int& sum, float& mean) {
	mean = sum / numbers.size();
	cout << "mean is : " << mean << endl;
}
void sortValue(vector<int>& numbers) { 
	cout << "Sorted value is : ";
	sort(numbers.begin(), numbers.end()); //uses sort(); function
	printValue(numbers);
	
} 
void medianValue(const vector<int>& numbers) {
	float medPos = numbers.size() / 2;
	cout << "Media position is : " << medPos << endl;
	cout << "Median value is : " << numbers[medPos] << endl;
}
void modeValue(const vector<int>& numbers) {
	int currentNumber = numbers[0];
	int mode = currentNumber;
	int count_newNumber = 1, count_oldNumber = 1;

	for (int i = 1; i<numbers.size(); i++) {
		if (numbers[i] == currentNumber) {
			count_newNumber++;
		} else {
			if (count_oldNumber <= count_newNumber) {
				count_oldNumber = count_newNumber;
				mode = currentNumber;
			}

			count_newNumber = 1;
			currentNumber = numbers[i];
		}
	}

	cout << "Mode value is : " << mode << endl;
}
void sdValue(const vector<int>& numbers, const float& mean, float& sd) {
	for (int i = 1; i < numbers.size(); i++) {
		sd = sd + (numbers[i]-mean)*(numbers[i]-mean);
	}
	sd = sqrt(sd / numbers.size());
	cout << "Standard Deviation value is : " << sd << endl;

}
void odd_evenValue(const vector<int>& numbers, int& odd, int& even) {

	for (int i = 0; i < numbers.size(); i++) {
		if ((numbers[i] % 2) == 0) { //if number is even
			even++;
		}
		else { //if number is odd
			odd++;
		}
	}
	cout << "Odd value is : " << odd << endl;
	cout << "Even value is : " << even << endl;
}
