# Change log #
### v.0.3 ###
* added Big-O comments. 
### v.0.2 ###
* fully working.
* most stable program I've ever code? [I think]
* added comments except Big-O. 
### v.0.1 ###
* add ability to generates vectors.
* can print vectors in function.
* can find summary value.
* can find min / max value.
* can find average value.
* can find even add odd value.
### v.0.0 ###
* Hello world.